package com.inobitec.orderservice2.repository;

import com.inobitec.orderservice2.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository {
    Order selectOrderById(Integer id);
    void insertOrder(Order order);
    void updateOrder(Integer id, Order order);
    void deleteOrder(Integer id);

}
