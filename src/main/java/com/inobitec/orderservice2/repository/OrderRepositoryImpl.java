package com.inobitec.orderservice2.repository;

import com.inobitec.orderservice2.mapper.OrderMapper;
import com.inobitec.orderservice2.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Order selectOrderById(Integer id) {
        return orderMapper.selectOrderById(id);
    }

    @Override
    public void insertOrder(Order order) {
        orderMapper.insertOrder(order);
    }

    @Override
    public void updateOrder(Integer id, Order order) {
        orderMapper.updateOrder(id, order);
    }

    @Override
    public void deleteOrder(Integer id) {
        orderMapper.deleteOrder(id);
    }
}
