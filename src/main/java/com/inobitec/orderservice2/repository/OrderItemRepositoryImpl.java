package com.inobitec.orderservice2.repository;

import com.inobitec.orderservice2.mapper.OrderItemMapper;
import com.inobitec.orderservice2.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderItemRepositoryImpl implements OrderItemRepository {

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Override
    public void insertOrderItem(OrderItem orderItem) {
        orderItemMapper.insertOrderItem(orderItem);
    }

    @Override
    public void updateOrderItem(Integer id, OrderItem orderItem) {
        orderItemMapper.updateOrderItem(id, orderItem);
    }

    @Override
    public void deleteOrderItem(Integer id) {
        orderItemMapper.deleteOrderItem(id);
    }
}
