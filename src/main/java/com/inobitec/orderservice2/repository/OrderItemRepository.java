package com.inobitec.orderservice2.repository;

import com.inobitec.orderservice2.model.OrderItem;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository {
    void insertOrderItem(OrderItem orderItem);
    void updateOrderItem(Integer id, OrderItem orderItem);
    void deleteOrderItem(Integer id);

}
