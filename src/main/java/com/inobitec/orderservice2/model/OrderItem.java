package com.inobitec.orderservice2.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderItem {

    private Integer id;
    private Integer orderId;
    private String itemName;
}