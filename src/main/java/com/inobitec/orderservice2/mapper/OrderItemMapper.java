package com.inobitec.orderservice2.mapper;

import com.inobitec.orderservice2.model.OrderItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderItemMapper {

    void insertOrderItem(@Param("oi") OrderItem orderItem);
    void updateOrderItem(@Param("id") Integer id, @Param("oi") OrderItem orderItem);
    void deleteOrderItem(@Param("id") Integer id);
}
