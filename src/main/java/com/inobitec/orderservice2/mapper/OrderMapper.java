package com.inobitec.orderservice2.mapper;

import com.inobitec.orderservice2.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderMapper {

    Order selectOrderById(@Param("id") Integer id);
    void insertOrder(@Param("o") Order order);
    void updateOrder(@Param("id") Integer id, @Param("o") Order order);
    void deleteOrder(@Param("id") Integer id);
}
