package com.inobitec.orderservice2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.inobitec.orderservice2.mapper")
public class OrderService2Application {

    public static void main(String[] args) {
        SpringApplication.run(OrderService2Application.class, args);
    }

}
