package com.inobitec.orderservice2.service;

import com.inobitec.orderservice2.model.OrderItem;
import com.inobitec.orderservice2.repository.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderItemServiceImpl implements OrderItemService{

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public void insertOrderItem(OrderItem orderItem) {
        orderItemRepository.insertOrderItem(orderItem);
    }

    @Override
    public void updateOrderItem(Integer id, OrderItem orderItem) {
        orderItemRepository.updateOrderItem(id, orderItem);
    }

    @Override
    public void deleteOrderItem(Integer id) {
        orderItemRepository.deleteOrderItem(id);
    }
}
