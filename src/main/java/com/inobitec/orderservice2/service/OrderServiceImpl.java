package com.inobitec.orderservice2.service;

import com.inobitec.orderservice2.model.Order;
import com.inobitec.orderservice2.model.OrderItem;
import com.inobitec.orderservice2.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemService orderItemService;

    @Override
    public Order selectOrderById(Integer id) {
        return orderRepository.selectOrderById(id);
    }

    @Override
    public void insertOrder(Order order) {
        orderRepository.insertOrder(order);
        for (OrderItem orderItem : order.getOrderItems()) {
            orderItem.setOrderId(order.getId());
            orderItemService.insertOrderItem(orderItem);
        }
    }

    @Override
    public void updateOrder(Integer id, Order order) {
        Order existingOrder = orderRepository.selectOrderById(id);
        if (existingOrder == null) {
            log.error("OrderService: order not found");
            throw new RuntimeException("Order not found");
        }
        orderRepository.updateOrder(id, order);
        for (OrderItem orderItem : order.getOrderItems()) {
            Integer orderItemId = orderItem.getId();
            if (orderItemId != null) {
                orderItemService.updateOrderItem(orderItemId, orderItem);
            } else {
                orderItem.setOrderId(id);
                orderItemService.insertOrderItem(orderItem);
            }
        }
        List<Integer> updateOrderItemIds = order.getOrderItems().stream().map(OrderItem::getId).collect(Collectors.toList());
        List<Integer> existingOrderItemIds = existingOrder.getOrderItems().stream().map(OrderItem::getId).collect(Collectors.toList());
        existingOrderItemIds.removeIf(updateOrderItemIds::contains);
        existingOrderItemIds.forEach(orderItemService::deleteOrderItem);
    }

    @Override
    public void deleteOrder(Integer id) {
        Order order = orderRepository.selectOrderById(id);
        for (OrderItem orderItem: order.getOrderItems()) {
            orderItemService.deleteOrderItem(orderItem.getId());
        }
        orderRepository.deleteOrder(id);
    }
}
