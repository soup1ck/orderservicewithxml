package com.inobitec.orderservice2.service;

import com.inobitec.orderservice2.model.OrderItem;
import org.springframework.stereotype.Service;

@Service
public interface OrderItemService {
    void insertOrderItem(OrderItem orderItem);
    void updateOrderItem(Integer id, OrderItem orderItem);
    void deleteOrderItem(Integer id);

}
