package com.inobitec.orderservice2.service;

import com.inobitec.orderservice2.model.Order;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {
    Order selectOrderById(Integer id);
    void insertOrder(Order order);
    void updateOrder(Integer id, Order order);
    void deleteOrder(Integer id);
}
