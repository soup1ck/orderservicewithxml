package com.inobitec.orderservice2.controller;

import com.inobitec.orderservice2.model.Order;
import com.inobitec.orderservice2.service.OrderItemService;
import com.inobitec.orderservice2.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    
    @GetMapping("/{id}")
    public Order selectOrderById(@PathVariable Integer id){
        return orderService.selectOrderById(id);
    }

    @PostMapping()
    public void createOrder(@RequestBody Order order){
        orderService.insertOrder(order);
    }

    @PutMapping("/{id}")
    public void updateOrder(@PathVariable Integer id,@RequestBody Order order){
        orderService.updateOrder(id,order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable Integer id){
        orderService.deleteOrder(id);
    }
}
